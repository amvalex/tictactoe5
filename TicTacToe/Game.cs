﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Game : IGame
    {
        private const byte FirstPlayer = 1;
        private const byte SecondPlayer = 2;
        private byte _currentPayer;
        private readonly int _maxPoints, _row, _column;
        private char[,] _table;
        public bool Finished { get; private set; }

        public Game(int maxPoints)
        {
            _maxPoints = maxPoints;
            _row = 10;
            _column = 10;
            _table = new char[_row,_column];
        }

        public void Start(byte playerNo)
        {
            if (playerNo != 1 && playerNo != 2)
            {
                Console.WriteLine("Wrong number.Player number must be 1 or 2!");
                return;
            }

            Console.WriteLine($"Starting game with player {playerNo}");
            _currentPayer = playerNo;
            _table[_row/2,_column/2] =  _currentPayer == FirstPlayer ? 'X' : 'O';
            Console.WriteLine($"{_table[_row/2,_column/2]} moved at 0,0.");
            _currentPayer = GetNextPlayerNo();
        }

        public byte GetNextPlayerNo()
        {
            return _currentPayer = (_currentPayer == FirstPlayer) ? SecondPlayer : FirstPlayer;
        }

        public void Move(int x, int y)
        {
            if (x < 0 || y < 0 || x > _row || y > _column)
            {
                Console.WriteLine("\nCoordinates error!");
                Console.WriteLine($"line must be between 0 and { _row }!");
                Console.WriteLine($"column must be between 0 and { _column }!\n");
                return;
            }

            if (_table[x, y] != 0)
            {
                Console.WriteLine($"\nError: Place already set into {_table[x, y]}\n");
                return;
            }

            _table[x, y] = _currentPayer == FirstPlayer ? 'X' : 'O';
            Console.WriteLine($"{_table[x, y]} moved at {x},{y}");
            _currentPayer = GetNextPlayerNo();
            int winner = GetWinner(x, y);
            //if (winner != 3)
            //{
            //    if (winner == 1)
            //    {
            //        Console.WriteLine("First player won!");
            //        Finished = true;
            //        return;
            //    }
            //    Console.WriteLine("Second player won!");
            //    Finished = true;
            //}
        }

        private int GetWinner(int x, int y)
        {
            int points = 0, aux_i = i, aux_j = j;

            // # 1 check vertically -------------------------
            // current + up
            while (points < _maxPoints &&
                   aux_i >= 1 &&
                   _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_i--;
            }
            aux_i = i + 1;
            // down
            while (points < _maxPoints &&
                   aux_i <= _y + 1 &&
                   _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_i++;
            }

            if (points == _maxPoints) return _table[i, j];
            // reset
            points = 0; aux_i = i; aux_j = j;

            // # 2 check horizontally -------------------------
            // current + left
            while (points < _maxPoints &&
                   aux_j >= 1 &&
                   _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_j--;
            }
            aux_j = j + 1;
            // right
            while (points < _maxPoints &&
                   aux_j <= _y + 1 &&
                   _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_j++;
            }

            if (points == _maxPoints) return _table[i, j];
            // reset
            points = 0; aux_i = i; aux_j = j;

            // # 3 check main diagonal -------------------------
            // current + up left
            while (points < _maxPoints &&
                   aux_i >= 1 &&
                   aux_j >= 1 &&
                   _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_i--;
                aux_j--;
            }
            aux_i = i + 1;
            aux_j = j + 1;
            // down right
            while (points < _maxPoints &&
                aux_i <= _y + 1 &&
                aux_j <= _y + 1 &&
                _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_i++;
                aux_j++;
            }

            if (points == _maxPoints) return _table[i, j];
            // reset
            points = 0; aux_i = i; aux_j = j;

            // # 4 check socondary diagonal -------------------------
            // current + up right
            while (points < _maxPoints &&
                   aux_i >= 1 &&
                   aux_j >= 1 &&
                   _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_i--;
                aux_j++;
            }
            aux_i = i + 1;
            aux_j = j - 1;
            // down left
            while (points < _maxPoints &&
                aux_i <= _y + 1 &&
                aux_j <= _y + 1 &&
                _table[i, j] == _table[aux_i, aux_j])
            {
                points++;
                aux_i++;
                aux_j--;
            }


            if (points == _maxPoints)
                return _table[i, j];

            return ' ';
        }
    }
}
