﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    interface IGame
    {
        /// <summary>
        /// Starts a new game and sets the first game move: the specified player sets its mark at position (0,0)
        /// </summary>
        /// <param name="playerNo">The number identifying the first player. Values: 1 or 2</param>
        void Start(byte playerNo);

        /// <summary>
        /// The number of the player that will make the next move
        /// </summary>
        byte GetNextPlayerNo();

        /// <summary>
        /// The current player sets its mark at position (x,y), relative to the (0,0) position.
        /// x and y can be positive or negative to allow any position around the first mark which is at (0,0).
        /// </summary>
        void Move(int x, int y);

        /// <summary>
        /// Indicates whether the game has a winner
        /// </summary>
        bool Finished { get; }
    }
}
