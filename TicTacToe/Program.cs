﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("TicTacToe - 5 to win \n");
            //var game = new Game(5);
            //Console.WriteLine("test - horiz right");
            //game.Start(1);
            //game.Move(9, 9);
            //game.Move(10, 11);
            //game.Move(9, 8);
            //game.Move(10, 12);
            //game.Move(9, 7);
            //game.Move(10, 13);
            //game.Move(9, 6);
            //game.Move(10, 14);

            //Console.WriteLine("Test - horiz left");
            //var game = new Game(5);
            //game.Start(1);
            //game.Move(9, 9);
            //game.Move(10, 9);
            //game.Move(9, 8);
            //game.Move(10, 8);
            //game.Move(9, 7);
            //game.Move(10, 7);
            //game.Move(9, 6);
            //game.Move(10, 6);

            //Console.WriteLine("Test - up");
            //var game = new Game(5);
            //game.Start(1);
            //game.Move(9, 9);
            //game.Move(11, 10);
            //game.Move(9, 8);
            //game.Move(12, 10);
            //game.Move(9, 7);
            //game.Move(13, 10);
            //game.Move(9, 6);
            //game.Move(14, 10);

            //Console.WriteLine("Test - down");
            //var game = new Game(5);
            //game.Start(1);
            //game.Move(9, 9);
            //game.Move(9, 10);
            //game.Move(9, 8);
            //game.Move(8, 10);
            //game.Move(9, 7);
            //game.Move(7, 10);
            //game.Move(9, 6);
            //game.Move(6, 10);

            //Console.WriteLine("Test - down right");
            //var game = new Game(5);
            //game.Start(1);
            //game.Move(9, 9);
            //game.Move(11, 11);
            //game.Move(9, 8);
            //game.Move(12, 12);
            //game.Move(9, 7);
            //game.Move(13, 13);
            //game.Move(9, 6);
            //game.Move(14, 14);

            //Console.WriteLine("Test - up left");
            //var game = new Game(5);
            //game.Start(1);
            //game.Move(2, 2);
            //game.Move(9, 9);
            //game.Move(2, 3);
            //game.Move(8, 8);
            //game.Move(2, 4);
            //game.Move(7, 7);
            //game.Move(2, 5);
            //game.Move(6, 6);

            Console.WriteLine("Bounds");
            var game = new Game(5);
            game.Start(1);
            game.Move(19, 19);
            game.Move(1, 0);
            game.Move(8, 8);
            game.Move(2, 0);
            game.Move(3, 4);
            game.Move(3, 0);
            game.Move(4, 5);
            game.Move(-1, 0);
            

            //Dictionary<string,int> dic = new Dictionary<string, int>();
            //dic["00"] = 1;
            //dic["-10"] = 2;
            //dic["0-1"] = 3;
            //dic["10"] = 4;
            
            Console.ReadKey(); 
        }
    }
}
